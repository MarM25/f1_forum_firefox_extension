create_shortlink();
set_last_post_date();

function create_shortlink(){
    var topic_has_id = false
    try{
        topic_has_id = document.getElementsByClassName('topic-title')[0].innerHTML.includes(23796)
    }
    catch(e){
        return
    }
    if (!topic_has_id && !window.location.href.includes(23796)){
        return;
    }

    new_header = document.createElement("h2");
    new_header.innerHTML = '<a href="https://www.formel1.de/forum/viewtopic.php?f=82&t=18978">Sünderkartei</a>';
    content = document.getElementById('page-body');
    content.prepend(new_header);
}

function set_last_post_date(){

    if (!window.location.href.includes("memberlist.php?mode=viewprofile&u=")){
        return;
    } 
    var urlParams = new URLSearchParams(window.location.search);
    var uid = urlParams.get('u');

    fetch("https://www.formel1.de/forum/search.php?author_id=" + uid + "&sr=posts")
    .then(function (response) {
        return response.text();
    })
    .then(function (data) {
        var doc = new DOMParser().parseFromString(data, "text/html");
        var first_post_date = doc.getElementsByClassName('search-result-date')[0];
        var last_ac = document.getElementsByClassName('details')[2].childNodes[7].innerHTML;
        document.getElementsByClassName('details')[2].childNodes[7].textContent = last_ac + " Letzter Post: " + first_post_date.innerHTML;
    })
    .catch(function (err) {
        console.warn('Something went wrong.', err);
    });

}


browser.runtime.onMessage.addListener(function (request) {
    replaceSelectedText(document.activeElement, request.text);
});

function replaceSelectedText(elem, text) {
    var start = elem.selectionStart;
    var end = elem.selectionEnd;
    elem.value = elem.value.slice(0, start) + text + elem.value.substr(end);
    elem.selectionStart = start + text.length;
    elem.selectionEnd = elem.selectionStart;
}