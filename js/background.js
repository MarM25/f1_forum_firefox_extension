browser.contextMenus.create({
    id: "log-selection",
    title: "Edit", // '%s' in str is marked stuff
    contexts: ["editable"],
    onclick: getClickHandler
});

function getClickHandler(info, tab) {
  browser.tabs.sendMessage(tab.id, {text: "[color=#008040][b]Edit[/b][/color]"});
};